
## SOA4ID

##### Proyecto Final - Sport TEC

Consta de dos partes, un servidor en Node.js y una applicación de Android que consume los servicios de este.  
La applicación y el servidor tienen el contexto de un sistema de control de noticias y de creación de retos deportivos en distintas diciplinas.

##### Software

La aplicación fue implementada en la versión 3.0.0 de Android studio.  

El servidor fue creado utilizando node.js y la versión 2.0 de loopback.

#### Instalación

Para correr la aplicación Android  se debe clonar el repositorio y correrlo en Android Studio para intalarlo en el celular.  

Para Poner en funcionamiento el Servidor clona el repositorio y en la carpeta de Server se instalan las dependencias con el comando npm install y se corre el servidor con el comando node.

#### Desarrollador

- Andrés Bris Chaves
