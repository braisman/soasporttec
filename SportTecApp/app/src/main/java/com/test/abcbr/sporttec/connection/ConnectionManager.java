package com.test.abcbr.sporttec.connection;

/**
 * Created by abcbr on 26/11/2017.
 */

public class ConnectionManager {

    private static String IP = "127.0.0.1";

    public static String getIP() {
        return IP;
    }

    public static void setIP(String IP) {
        ConnectionManager.IP = IP;
    }
}
