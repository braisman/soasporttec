package com.test.abcbr.sporttec.user;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.MainActivity;
import com.test.abcbr.sporttec.R;
import com.test.abcbr.sporttec.connection.ConnectionManager;
import com.test.abcbr.sporttec.models.User;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private View view;

    private Button mBtnLogin;

    private TextView mTxtvToSignup;

    private EditText mEmail;
    private EditText mPassword;

    private Context context;

    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;

        view = findViewById(R.id.content);
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                System.out.println("se hizo un long click");

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("IP Change");
                final EditText edtxtIp = new EditText(context);
                builder.setView(edtxtIp);
                builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConnectionManager.setIP(edtxtIp.getText().toString());
                    }
                });
                builder.setNegativeButton("Not Change", null);
                builder.show();

                return false;
            }
        });
        //load components
        mBtnLogin = findViewById(R.id.login_btn);
        mTxtvToSignup = findViewById(R.id.login_to_sign_up);

        mEmail = findViewById(R.id.login_email);
        mPassword = findViewById(R.id.login_password);
        //listener
        mBtnLogin.setOnClickListener(this);
        mTxtvToSignup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_btn:
                login();
                break;
            case R.id.login_to_sign_up:
                toSignup();
                break;
            default:

                break;
        }
    }

    private void login(){

        boolean flag = true;

        email = mEmail.getText().toString();
        password = mPassword.getText().toString();

        if(email.isEmpty()){
            mEmail.setError("Email field is necessary");
            flag = false;
        }
        if(password.isEmpty()){
            mPassword.setError("Password field is necessary");
            flag = false;
        }

        if(flag){
            Ion.with(this)
                    .load("GET", "http://" + ConnectionManager.getIP() + User.USER_PATH + "?filter={\"where\":{\""+User.USER_EMAIL+"\":\""+ email + "\"}}")
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray result) {
                            if(result != null){
                                toMainActivity(result);
                            }
                        }
                    });
        }


    }

    private void toMainActivity(JsonArray result){

        if(result.size() > 0){

            Gson gson = new Gson();

            String password_saved  = result.get(0).getAsJsonObject().get(User.USER_PASSWORD).getAsString();

            User user = gson.fromJson(result.get(0).getAsJsonObject(), User.class);

            if(password.compareTo(password_saved) == 0){
                Intent loginIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(loginIntent);
            } else {
                mPassword.setError("The password is incorrect");
            }

        } else {
            mEmail.setError("There is no user with this email");
        }
    }

    private void toSignup(){
        Intent loginIntent = new Intent(getApplicationContext(), SignupActivity.class);
        startActivity(loginIntent);
    }
}
