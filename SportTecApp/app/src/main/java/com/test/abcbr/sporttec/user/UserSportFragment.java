package com.test.abcbr.sporttec.user;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.abcbr.sporttec.R;
import com.test.abcbr.sporttec.models.Sport;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnSportItemSelected}
 * interface.
 */
public class UserSportFragment extends Fragment implements  OnSportItemSelected{

    private int mColumnCount = 2;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UserSportFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_sports, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            Sport sport1 = new Sport();
            sport1.setmName("Football");
            sport1.setmImageUrl("https://i.ebayimg.com/images/g/xaMAAOSwRLZT3sZY/s-l300.jpg");

            Sport sport2 = new Sport();
            sport2.setmName("Basketball");
            sport2.setmImageUrl("https://ih1.redbubble.net/image.105177954.9599/flat,800x800,075,f.jpg");

            List<Sport> sports = new ArrayList<>();

            sports.add(sport1);
            sports.add(sport2);

            recyclerView.setAdapter(new MySportItemRecyclerViewAdapter(sports, this));
        }
        return view;
    }

    @Override
    public void onItemSportCheckedChange(Sport item, boolean isChecked) {
        System.out.println("Nombre del deporte: " + item.getmName() + " estado: " + Boolean.toString(isChecked));
    }
}
